---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Automated Security Findings Management: A Case Study in Industrial DevOps"
authors: ["Markus Voggenreiter", "Florian Angermeir", "Fabiola Moyón", "Ulrich Schöpp", "Pierre Bonvin"]
date: 2024-01-12T15:45:00Z
doi: "https://doi.org/10.1145/3639477.3639744"

# Schedule page publish date (NOT publication's date).
#publishDate: 2024-01-12T15:45:00Z

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "In recent years, DevOps, the unification of development and operation workflows, has become a trend for the industrial software development lifecycle. Security activities turned into an essential field of application for DevOps principles as they are a fundamental part of secure software development in the industry. A common practice arising from this trend is the automation of security tests that analyze a software product from several perspectives. To effectively improve the security of the analyzed product, the identified security findings must be managed and looped back to the project team for stakeholders to take action. This management must cope with several challenges ranging from low data quality to a consistent prioritization of findings while following DevOps aims. To manage security findings with the same efficiency as other activities in DevOps projects, a methodology for the management of industrial security findings minding DevOps principles is essential.

In this paper, we propose a methodology for the management of security findings in industrial DevOps projects, summarizing our research in this domain and presenting the resulting artifact. As an instance of the methodology, we developed the Security Flama, a semantic knowledge base for the automated management of security findings. To analyze the impact of our methodology on industrial practice, we performed a case study on two DevOps projects of a multinational industrial enterprise. 
The results emphasize the importance of using such an automated methodology in industrial DevOps projects, confirm our approach's usefulness and positive impact on the studied projects, and identify the communication strategy as a crucial factor for usability in practice."

# Summary. An optional shortened abstract.
summary: ""

tags: ["DevOps", "Security", "Software Engineering", "Security Findings Management"]
categories: ["Software Engineering"]
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://arxiv.org/pdf/2401.06602.pdf"
# url_code: "https://zenodo.org/record/4106329"
# url_dataset:
# url_poster:
# url_project:
# url_slides:
# url_source:
#url_video: "https://youtu.be/Z4zBeKUsquo"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#  caption: ""
#  focal_point: ""
#  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: ""
---
