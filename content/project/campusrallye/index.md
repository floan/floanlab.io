---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Campusrallye"
summary: "Organization of the Campusrallye @FSMPI"
draft: true 
authors: []
tags: []
categories: []
date: 2020-09-07T18:36:07+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.

url_code: "https://git.lamf.de/SET/Rallye-Server"

---

## Introduction
Every wintersemester the [FSMPI](https://mpi.fs.tum.de/en/) offers the **Study Introduction Days** for new first year students. As part of the Study Introduction Days they also organize a **Campusrallye** to help students find their way through the campus and make first friends.

The campusrallye consists of stations distributed across the campus in Garching. Each station offers a challenge like Jeopardy, decrypting an encrypted message or balancing on a slackline.
Students form groups of 4-6 people to solve these challenges. Each challenge is choosen and placed on the campus in a way, that fosters teamwork and increases orientation. The faster a challenges is solved the more points the respective groups receives. After two to three hours the rallye ends with a winning ceremony.

## How did we organize it?
### Preparation
1. Assemble organization team \o/
2. Define challenges
   - Is it feasible?
   - Might it become a cultural/religious problem?
   - 
3. Decide where to place challenges
   - What to be careful about?
   -  
4. 

### During Campusrallye
- 
- 
- 

### 

## Disclaimer
I was part of the campusrallye organization team from 2016 till 2020, so I can only speak about that timerange.


