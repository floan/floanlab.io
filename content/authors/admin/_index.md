---
# Display name
title: Florian Angermeir

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Researcher

# Organizations/Affiliations
organizations:
- name: fortiss
  url: "https://fortiss.org"

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- Compliance & Automation
- DevSecOps & Security in agile Processes
- Data Analysis

education:
  courses:
  - course: Ph.D. 
    institution: Blekinge Institute of Technology
    year: 2023-Today
  - course: MSc in Computer Science 
    institution: Technical University of Munich
    year: 
  - course: BSc in Computer Science 
    institution: Technical University of Munich 
    year: 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:website@angermeir.me'  # For a direct email link, use "mailto:test@example.org".
- icon: mastodon
  icon_pack: fab
  link: https://layer8.space/@angrymeir
- icon: github
  icon_pack: fab
  link: https://github.com/angrymeir
- icon: gitlab
  icon_pack: fab
  link: https://git.lamf.de/flo

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
#user_groups:
#- Researchers
#- Visitors
---

<br><b>Hey there!</b>   

I am a researcher at [fortiss](https://fortiss.org), and a PhD student at [BTH](https://www.bth.se). My work primarily focuses on the automation aspects of compliance in continuous software development projects, its challenges and their implications in practice.

Besides compliance solutions for industry I have a keen interest in comprehending the implicit challenges of the way we perform science nowadays. The most recent topic my thoughts revolve around is the replication crisis in psychology and the potential takeaways it offers for the software engineering research community. If you have thoughts on this topic don't hesitate to drop me a message!

In my spare-time I'm an experienced binge-watcher, passionate gymgoer, and recently started to enjoy the underwater world while scuba diving.
