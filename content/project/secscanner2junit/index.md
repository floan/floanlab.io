---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "SecScanner2JUnit"
summary: "GitLab offers security scanning and visualization directly via and on their platform. One nice feature is direct insights on merge requests. However, this feature is only available with the Ultimate tier. SecScanner2JUnit converts security scanning outputs to JUnit reports, so that they can be visualized in a similar manner."
authors: [Florian Angermeir]
tags: []
categories: []
date: 2023-01-05T16:04:10Z

# Optional external URL for project (replaces project detail page).
external_link: "https://github.com/angrymeir/SecScanner2JUnit"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: "https://github.com/angrymeir/SecScanner2JUnit"
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
