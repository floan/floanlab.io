---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Towards Automated Continuous Security Compliance"
event: "Empirical Software Engineering and Measurement 2024"
event_url: "https://conf.researchr.org/details/esem-2024/esem-2024-emerging-results-vision-and-reflection-papers/8/Towards-Automated-Continuous-Security-Compliance"
location: "Barcelona, Spain"
address:
  street:
  city:
  region:
  postcode:
  country:
summary: This talk summarizes the paper *"Towards Automated Continuous Security Compliance"*. It covers three contribution 1. a definition of "Continuous Security Compliance", 2. a brief explanation why automation will be a key enabler for continuous security compliance, and 3. a research design to address the gap between security compliance and continuous software engineering projects.
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2024-10-24T16:40:00Z
# date_end: 2023-01-05T14:33:37Z
# all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2023-01-05T13:00:00Z

authors: [Florian Angermeir]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: media/2024_10_24_ESEM_Vision_Paper.pdf 
#https://gitlab.com/floan/floan.gitlab.io/-/blob/master/static/media/Security\_in\_IaC.pdf

url_code: 
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
