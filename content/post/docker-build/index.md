---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "GitLab CI: Docker Build, Scan and Push"
subtitle: "Build a docker container with Kaniko, scan it with Trivy and push it with Crane"
summary: ""
authors: []
tags: []
categories: [CI, Security]
date: 2021-03-01T16:21:11Z
lastmod: 2021-03-01T16:21:11Z
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

## Motivation
Suppose you develop and deploy your application using docker. A common problem of development with docker is building, scanning and pushing a docker image on infrastructure that is dockerized itself (e.g. CI buildagents).

In this post we:
- discover how to build docker images in a containerized environment
- briefly look at one way to scan locally build, but not yet pushed docker images
- see how to push a docker image to a registry
and all of that in the context of GitLab CI pipelines!

## Building a docker image
### The ugly way
You might wonder why not directly build a docker image using `docker build ...`.
So lets write a pipeline that build a docker image:
```yaml
docker_in_dockerized:
	stage: build
	image: alpine
	script:
		- apk --no-cache add docker
		- docker build . -t test
```
The pipeline would fail with the following error message:
```bash
$ docker build . -t test
Cannot connect to the Docker daemon at tcp://localhost:2375. Is the docker daemon running?
```

#### Why is that and how can we solve it?
In order to invoke an action

Let's have a look at possible solutions:

##### 1. Mount the docker socket
Mount the docker socket.
- mount /var/run/docker.sock
- Why not do it?

##### 2. Docker in Docker
- dind (Docker in Docker) requires the container to be privileged
- Why not do it?

##### 3. Other options
There are a few other options which I don't want to go into here.

### The pretty way
- Use kaniko
- It this safe now?
- Security considerations
```yaml
docker:
    stage: build
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    script: 
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --no-push --destination=image --tarPath=image.tar
    artifacts:
        paths:
            - image.tar
        when: on_success
```

## Scan a docker image locally
- Change output of kaniko to save tar
- Hand over artifact to next stage
- Run trivy on local image
- Give example
- Considerations?
```yaml
vulnerability:
    stage: test
    image:
        name: aquasec/trivy:latest
        entrypoint: [""]
    script:
        - trivy --format json --output trivy.json --exit-code 1 --input image.tar
    allow_failure: true
    artifacts:
        paths:
            - trivy.json
        when: on_failure
```

## Push docker image
- Docker push?
- Crane
- Give example
- Is this safe now?
```yaml
push:
    stage: push
    image: 
        name: gcr.io/go-containerregistry/crane:debug
        entrypoint: [""]
    script:
        - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - crane push image.tar $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
```


## Final thoughts
- Use it or not?
- Is this safer now?
