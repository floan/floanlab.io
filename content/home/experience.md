+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Research Associate"
  company = "fortiss"
  company_url = "https://fortiss.org"
  location = "Munich"
  date_start = "2023-09-01"
  date_end = ""
  description = """"""

[[experience]]
  title = "Working Student"
  company = "Siemens Technology"
  company_url = ""
  location = "Munich"
  date_start = "2018-11-01"
  date_end = "2023-08-31"
  description = """Research on Security in Continuous Software Engineering"""

[[experience]]
  title = "System Administrator"
  company = "Chair for Decentralized Systems Engineering"
  company_url = "https://dse.in.tum.de"
  location = "Munich"
  date_start = "2020-10-01"
  date_end = "2022-07-31"
  description = """"""

[[experience]]
  title = "System Administrator"
  company = "Chair for Applied Software Engineering"
  company_url = "https://ase.in.tum.de"
  location = "Munich"
  date_start = "2017-04-01"
  date_end = "2020-09-30"
  description = """"""

+++
